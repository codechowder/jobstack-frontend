import { JobstackFrontendPage } from './app.po';

describe('jobstack-frontend App', () => {
  let page: JobstackFrontendPage;

  beforeEach(() => {
    page = new JobstackFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
