import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { AppComponent } from './app.component';
import { AuthenticationService, AlertService } from './services/index';
import { AlertComponent } from './directives/index';
import { AuthGuard } from './guards/index';
import { routing } from './app.routing';
import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent
  ],
  imports: [
    MaterialModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [
    AuthenticationService,
    AuthGuard,
    AlertService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
