import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { AuthResponse } from '../models/index';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
  /**
   * Default Constructor
   * @param http 
   */
  constructor(private http: Http) {
    // Nothing to do.
  }

  /**
   * Registers a new user.
   * @param username 
   * @param password 
   */
  public register(username: string, password: string): Observable<AuthResponse> {
    return this.http.post('/register', JSON.stringify({ username: username, password: password }))
      .map((response: Response) => {
        let res = response.json() as AuthResponse;
        return res;
      });
  }

  /**
   * Logs the user into the system.
   * @param username 
   * @param password 
   */
  public login(username: string, password: string): any {
    return this.http.post('/login', JSON.stringify({ username: username, password: password }))
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        let user = response.json();
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }
      });
  }

  /**
   * Logs the user out of the system.
   */
  public logout(): void {
    // remove user from local storage to log user out
    this.http.get('/logout')
      .map((response: Response) => {
        localStorage.removeItem('currentUser');
      });
  }
}